# Team 11

## Multilingo Translation Portal

### Description

MultiLingo is text translation app which could be used to show the content of a web site (for example a knowledge sharing portal/wiki) in multiple languages.
We used mechanism for automatic population of the translations using google-translate-api-browser
on insert or update of a text.


## Server side

 * If you want to use our server here is link - https://gitlab.com/i_kadieva/multilingo
    
### Client

  * Home page
    - Here is where all the shared articles are visible in predefined for the user’s browser locale language.  
    ![Image description](screenshots/home.png)

 * Register page 
    ![Image description](screenshots/register.png)
    
 * Login page 
    - When you login in you account there is automatic language change to the preferred one you choose when you registered. 
    ![Image description](screenshots/login.png)
    
 * Home page functionality
    
    - Here is where all the shared articles are visible in predefined for the user’s browser locale language.
    ![Image description](screenshots/functionality1.png)
    
   1 You can change web-site language.
   
   2 Create articles if you are logged user.
   
   3 If you are admin you can go to admin-panel.
   
   4 If you are admin or editor you can go to translations page and edit translations.
   
   5 This is logout.
   
 * Translation page 
     
     - Here you can edit translation
    
    ![Image description](screenshots/translations.png)
    
 * Admin panel page functionality
     
     - Language part of admin panel.
    
     ![Image description](screenshots/langage-panel.png)
     
    1 Hero you can go to user part of admin panel.
    
    2 From here you add new languages. When you add new language the whole website is translated and you can choose the new language and set it as default from the "Change language" button in the top-left corner.

* Admin panel page functionality
     
     - Users part of admin panel.
    
     ![Image description](screenshots/users-panel.png)
     
    1 Here you can go to language part of admin panel.
    
    2 From here you delete user.
    
    3 Buttons for add and remove roles.
    
* Edit article page (only for admin and registered users)
     
     - Here you can edit articles
    
    ![Image description](screenshots/edit-article.png)
    
* Articles version page (only for admin and registered users)
    ![Image description](screenshots/version1.png)
     
    1 Here you can set current version of article.
    
    2 This opens view article window.
    
    3 Opened view article window.

### Technologies
    
   * JavaScript, 
   * TypeScript, 
   * NestJS, 
   * Angular 8, 
   * TypeORM, 
   * mySQL, 
   * Jest, 
   * HTML, 
   * CSS, 
   * Ngx-toaster
   * MDBootstrap
   
### Authors and Contributors
    
  ##### - Irena Kadieva - https://gitlab.com/i_kadieva
  ##### - Stoyan Smilyanov - https://gitlab.com/Stoyan24

      
### License

   - This project is licensed under MIT License
