import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserDTO} from './users/models/user.dto';
import {NotificationService} from './core/services/notification.service';
import {AuthService} from './core/services/auth.service';
import {Subscription} from 'rxjs';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  public loggedUser: UserDTO;
  private loggedUserSubscription: Subscription;

  constructor(
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly notification: NotificationService,
  ) {
  }

  public ngOnInit(): void {
    this.loggedUserSubscription = this.authService.loggedUserData$.subscribe(
      (data: UserDTO) => this.loggedUser = data
    );
  }

  public ngOnDestroy(): void {
    this.loggedUserSubscription.unsubscribe();
  }
}
