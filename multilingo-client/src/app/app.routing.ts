import { ArticlesResolverService } from './core/services/articles-resolver.service';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { NotFoundPageComponent } from './components/not-found-page/not-found-page.component';
import { ServerErrorPageComponent } from './components/server-error-page/server-error-page.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent, resolve: { articles: ArticlesResolverService }, runGuardsAndResolvers: 'always' },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  {
    path: 'articles',
    loadChildren: () => import('./articles/articles.module').then(m => m.ArticlesModule),
  },
  {
    path: 'translations',
    loadChildren: () => import('./translations/translations.module').then(m => m.TranslationsModule),
  },
  {
    path: 'versions',
    loadChildren: () => import('./versions/versions.module').then(m => m.VersionsModule),
  },
  {
    path: 'admin-panel',
    loadChildren: () => import('./admin-panel/admin-panel.module').then(m => m.AdminPanelModule),
  },
  { path: 'not-found', component: NotFoundPageComponent },
  { path: 'server-error', component: ServerErrorPageComponent },
  { path: '**',  redirectTo: '/not-found' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      onSameUrlNavigation: 'reload',
    })
  ],
  exports: [
    RouterModule,
  ],
})
export class AppRoutingModule { }
