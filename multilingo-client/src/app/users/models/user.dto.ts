import { RoleDTO } from './role.dto';
import {LanguageDto} from '../../shared/languages/models/language.dto';

export class UserDTO {

  public id: string;
  public username: string;
  public email: string;
  public avatarPath: string;
  public roles: string[];
  public preferedLangCode?: LanguageDto;
}
