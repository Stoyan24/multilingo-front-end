import { ArticlesService } from './../../core/services/articles.service';
import { ArticleDTO } from './../models/articles.dto';
import { NotificationService } from './../../core/services/notification.service';
import { LanguagesService } from './../../core/services/languages.service';
import { Component, OnInit, Output, EventEmitter, Input, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs';
import { CurrentLanguageDTO } from '../../shared/languages/models/current-language.dto';
import { EditArticleDTO } from '../models/edit-article';
import { UiElementsDto } from '../../shared/ui-elements/models/ui-elements.dto';
import { UIelFinderPipe } from '../../core/pipes/uiel-finder.pipe';
@Component({
  selector: 'app-edit-article',
  templateUrl: './edit-article.component.html',
  styleUrls: ['./edit-article.component.scss']
})
export class EditArticleComponent implements OnInit, OnDestroy {

  @Input() public uiElements: UiElementsDto[];
  @Input() public article: ArticleDTO;
  @Output() public oNEdit: EventEmitter<ArticleDTO> = new EventEmitter<ArticleDTO>();
  public editArticleForm: FormGroup;
  private currentLanguage: CurrentLanguageDTO;
  private currentLanguageSubscription$: Subscription;
  public language: string;

  constructor(
    public readonly formBuilder: FormBuilder,
    private readonly articleService: ArticlesService,
    public readonly notification: NotificationService,
    public readonly languageService: LanguagesService,
    private readonly UIelFinder: UIelFinderPipe,
  ) { }

  ngOnInit() {
    this.currentLanguageSubscription$ = this.languageService.currentLanguageCode$
      .subscribe((language: CurrentLanguageDTO) => {
        this.language = language.code;
      });

    this.editArticleForm = this.formBuilder.group({
      imgPath: [this.article.versions[0].imgPath],
      title: [(this.article.versions[0].title as any).originalText || this.article.versions[0].title],
      content: [(this.article.versions[0].content as any).originalText || this.article.versions[0].content],
    });
  }

  public emitData(editData: EditArticleDTO) {
    const articleToEdit = { ...editData, languageCode: this.article.versions[0].languageCode };
    this.articleService.editArticle(articleToEdit, this.article.id)
      .subscribe(
        (article) => {
          this.article.versions = [article.versions.find(version => version.isCurrent === true)];
          this.notification.success(this.UIelFinder.transform(this.uiElements, 'article_updated'));
          this.oNEdit.emit(this.article);
          this.editArticleForm.setValue({
            imgPath: [this.article.versions[0].imgPath],
            title: [this.article.versions[0].title],
            content: [this.article.versions[0].content],
          });
        },
        (error) => this.notification.error(error.error.error),
      );
  }

  ngOnDestroy() {
    this.currentLanguageSubscription$.unsubscribe();
  }
}
