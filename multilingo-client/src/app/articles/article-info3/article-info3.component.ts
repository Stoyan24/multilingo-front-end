import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { ArticleDTO } from '../models/articles.dto';
import { Subscription } from 'rxjs';
import { AuthService } from './../../core/services/auth.service';
import { UserDTO } from './../../users/models/user.dto';
import { UiElementsDto } from '../../shared/ui-elements/models/ui-elements.dto';

@Component({
  selector: 'app-article-info3',
  templateUrl: './article-info3.component.html',
  styleUrls: ['./article-info3.component.scss']
})
export class ArticleInfo3Component implements OnInit, OnDestroy {

  @Input() public uiElements: UiElementsDto[];
  @Input() public article: ArticleDTO;
  @Output() public oNDelete: EventEmitter<string> = new EventEmitter<string>();
  private loggedUserSubscription: Subscription;
  public loggedUserData: UserDTO;

  constructor(
    private readonly authService: AuthService,
  ) { }

  ngOnInit() {
    this.loggedUserSubscription = this.authService.loggedUserData$.subscribe(
      (data: UserDTO) => this.loggedUserData = data,
    );
  }

  public deleteArticle(deleteArticle: boolean) {
    if (deleteArticle) { this.oNDelete.emit(this.article.id); }
  }

  ngOnDestroy() {
    this.loggedUserSubscription.unsubscribe();
  }

}
