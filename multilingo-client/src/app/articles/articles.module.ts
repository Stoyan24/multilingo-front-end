import { VersionsModule } from './../versions/versions.module';
import { CoreModule } from './../core/core.module';
import { ArticlesResolverService } from '../core/services/articles-resolver.service';
import { ArticlesRoutingModule } from './articles.routing';
import { CreateArticleComponent } from './create-article/create-article.component';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { ArticleResolverService } from './services/article-resolver.service';
import { ArticleDetailsComponent } from './article-details/article-details.component';
import { ArticleInfo1Component } from './article-info1/article-info1.component';
import { ArticleInfo2Component } from './article-info2/article-info2.component';
import { ArticleInfo3Component } from './article-info3/article-info3.component';
import { ArticleInfo4Component } from './article-info4/article-info4.component';
import { EditArticleComponent } from './edit-article/edit-article.component';
import { AllArticlesComponent } from './all-articles/all-articles.component';



@NgModule({
  declarations: [
    AllArticlesComponent,
    CreateArticleComponent,
    ArticleDetailsComponent,
    ArticleInfo1Component,
    ArticleInfo2Component,
    ArticleInfo3Component,
    ArticleInfo4Component,
    EditArticleComponent,
  ],
  providers: [
    ArticlesResolverService,
    ArticleResolverService,
  ],
  imports: [
    ArticlesRoutingModule,
    CoreModule,
    SharedModule,
    VersionsModule,
  ],
  exports: [
    AllArticlesComponent,
  ],
})
export class ArticlesModule { }
