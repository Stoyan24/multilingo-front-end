import { CurrentLanguageDTO } from './../../shared/languages/models/current-language.dto';
import { NotificationService } from 'src/app/core/services/notification.service';
import { LanguagesService } from 'src/app/core/services/languages.service';
import { ArticlesService } from './../../core/services/articles.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ArticleDTO } from '../models/articles.dto';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ArticleResolverService {

  constructor(
    private readonly router: Router,
    private readonly articleService: ArticlesService,
    public readonly notification: NotificationService,
    public readonly languageService: LanguagesService,
  ) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<ArticleDTO> {

    let languageCode: string;
    const id = route.params.id;
    this.languageService.currentLanguageCode$
      .subscribe(
        (language: CurrentLanguageDTO) => languageCode = language.code,
        (error) => this.notification.error(error.error.error),
      ).unsubscribe();

    return this.articleService.getArticleById(languageCode, id)
      .pipe(
        map(article => {
          if (article) {
            return article;
          } else {
            this.router.navigate(['/not-found']);
            this.notification.error(`There was an unexpected error.`);
            return;
          }
        })
      );
  }
}
