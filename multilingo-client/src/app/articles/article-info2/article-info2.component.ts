import { Component, Input, Output, EventEmitter, OnInit, OnDestroy } from '@angular/core';
import { ArticleDTO } from '../models/articles.dto';
import { AuthService } from './../../core/services/auth.service';
import { UserDTO } from './../../users/models/user.dto';
import { Subscription } from 'rxjs';
import { UiElementsDto } from '../../shared/ui-elements/models/ui-elements.dto';

@Component({
  selector: 'app-article-info2',
  templateUrl: './article-info2.component.html',
  styleUrls: ['./article-info2.component.scss']
})
export class ArticleInfo2Component implements OnInit, OnDestroy {

  @Input() public uiElements: UiElementsDto[];
  @Input() public article: ArticleDTO;
  @Output() public oNDelete: EventEmitter<string> = new EventEmitter<string>();
  private loggedUserSubscription: Subscription;
  public loggedUserData: UserDTO;

  constructor(
    private readonly authService: AuthService,
  ) { }

  ngOnInit() {
    this.loggedUserSubscription = this.authService.loggedUserData$.subscribe(
      (data: UserDTO) => this.loggedUserData = data,
    );
  }

  public deleteArticle(deleteArticle: boolean) {
    if (deleteArticle) { this.oNDelete.emit(this.article.id); }
  }

  ngOnDestroy() {
    this.loggedUserSubscription.unsubscribe();
  }

}
