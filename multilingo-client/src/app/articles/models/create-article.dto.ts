export class CreateArticleDTO {

  public imgPath: string;
  public title: string;
  public content: string;
  public languageCode?: string;

}
