export class EditArticleDTO {

  public imgPath: string;
  public title: string;
  public content: string;

}
