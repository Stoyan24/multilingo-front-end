import { VersionDTO } from './../../versions/models/version.dto';
import { UserDTO } from './../../users/models/user.dto';
export class ArticleDTO {

  public id: string;
  public createdOn: Date;
  public updatedOn: Date;
  public isDeleted?: boolean;
  public versions: VersionDTO[];
  public createdBy: UserDTO;

}
