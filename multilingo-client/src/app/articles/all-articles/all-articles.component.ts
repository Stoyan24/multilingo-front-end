import { RoleDTO } from './../../users/models/role.dto';
import { AuthService } from './../../core/services/auth.service';
import { UserDTO } from './../../users/models/user.dto';
import { LanguagesService } from './../../core/services/languages.service';
import { CurrentLanguageDTO } from './../../shared/languages/models/current-language.dto';
import { ArticleDTO } from './../models/articles.dto';
import { NotificationService } from './../../core/services/notification.service';
import { ArticlesService } from './../../core/services/articles.service';
import { CreateArticleDTO } from './../models/create-article.dto';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { UiElementsDto } from '../../shared/ui-elements/models/ui-elements.dto';
import { UiElementsService } from '../../core/services/ui-elements.service';
import { UIelFinderPipe } from '../../core/pipes/uiel-finder.pipe';

@Component({
  selector: 'app-all-articles',
  templateUrl: './all-articles.component.html',
  styleUrls: ['./all-articles.component.scss']
})
export class AllArticlesComponent implements OnInit, OnDestroy {

  public articles: ArticleDTO[] = [];
  public articlesPerPage: ArticleDTO[] = [];
  public page = 1;
  public currentPage = 1;
  public previousPage: number;
  public readonly newArticlesCount = 5;
  public readonly pageSize = 15;
  private currentLanguage: CurrentLanguageDTO;
  private currentLanguageSubscription: Subscription;
  private loggedUserSubscription: Subscription;
  public loggedUserData: UserDTO;
  public uiElements: UiElementsDto[];
  private uiElementsSubscription: Subscription;

  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly articleService: ArticlesService,
    private readonly authService: AuthService,
    private readonly languageService: LanguagesService,
    private readonly notification: NotificationService,
    private readonly uiElementsService: UiElementsService,
    private readonly UIelFinder: UIelFinderPipe,
  ) { }

  ngOnInit() {
    this.uiElementsSubscription = this.uiElementsService.uiElementsData$.subscribe((data) => {
      this.uiElements = data;
    });
    this.route.data.subscribe(
      (data) => {
        this.articles = data.articles;
        this.loadData();
      },
    );
    if (this.articles.length === 0) {
      this.notification.warning(this.UIelFinder.transform(this.uiElements, 'no_articles'));
    }
    this.currentLanguageSubscription = this.languageService.currentLanguageCode$
      .subscribe((language: CurrentLanguageDTO) => {
        this.currentLanguage = language;
        this.articleService.getAllArticles(this.currentLanguage.code)
          .subscribe(
            (data) => {
              this.articles = data;
              this.loadData();
            },
            (error) => this.notification.error(error.error.error),
          );
      });
    this.loggedUserSubscription = this.authService.loggedUserData$.subscribe(
      (data: UserDTO) => this.loggedUserData = data,
    );
  }

  public setCurrentPage(page: number) {
    this.currentPage = page;
  }

  public loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.loadData();
    }
  }

  public loadData() {
    this.articlesPerPage = this.articles.slice(
      (this.page - 1) * this.pageSize + this.newArticlesCount,
      (this.page - 1) * this.pageSize + this.newArticlesCount + this.pageSize,
    );
  }

  public createArticle(articleToCreate: CreateArticleDTO) {
    this.articleService.createArticle(articleToCreate)
      .subscribe(
        (article) => {
          if (article.versions[0].languageCode === this.currentLanguage.code) {
            this.articles.unshift(article);
          } else {
            this.articleService.getArticleById(this.currentLanguage.code, article.id)
              .subscribe(
                (data) => this.articles.unshift(data),
                (error) => this.notification.error(error.error.error),
              );
          }
          this.loadPage(this.currentPage);
          this.notification.success(this.UIelFinder.transform(this.uiElements, 'article_created'));
        },
        (error) => this.notification.error(error.error.error),
      );
  }

  public deleteArticle(id: string, page: number) {
    this.articleService.deleteArticle(id)
      .subscribe(
        (articleData) => {
          this.articles = this.articles.filter(article => article.id !== articleData.id);
          if (this.loggedUserData.roles.includes('Administrator')) {
            if (articleData.versions[0].languageCode === this.currentLanguage.code) {
              this.articles.unshift(articleData);
            } else {
              this.articleService.getArticleById(this.currentLanguage.code, articleData.id)
                .subscribe(
                  (data) => this.articles.unshift(data),
                  (error) => this.notification.error(error.error.error),
                );
              this.articles.unshift(articleData);
            }

            this.loadPage(this.currentPage);
            this.notification.success(this.UIelFinder.transform(this.uiElements, 'article_deleted'));
          }
        },
        (error) => this.notification.error(error.error.error),
      );
  }

  ngOnDestroy() {
    this.currentLanguageSubscription.unsubscribe();
    this.loggedUserSubscription.unsubscribe();
    this.uiElementsSubscription.unsubscribe();
  }

}
