import { AuthService } from './../../core/services/auth.service';
import { UserDTO } from './../../users/models/user.dto';
import { TranslationsService } from './../../core/services/translations.service';
import { ActivatedRoute } from '@angular/router';
import { ArticlesService } from './../../core/services/articles.service';
import { ArticleDTO } from './../models/articles.dto';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { LanguagesService } from 'src/app/core/services/languages.service';
import { NotificationService } from 'src/app/core/services/notification.service';
import { CurrentLanguageDTO } from '../../shared/languages/models/current-language.dto';
import { RatingDTO } from '../../shared/rating/models/rating.dto';
import { ArticleTranslationDTO } from '../../translations/model/article-translation.dto';
import { UiElementsDto} from '../../shared/ui-elements/models/ui-elements.dto';
import { UiElementsService} from '../../core/services/ui-elements.service';
import { UIelFinderPipe } from '../../core/pipes/uiel-finder.pipe';

@Component({
  selector: 'app-article-details',
  templateUrl: './article-details.component.html',
  styleUrls: ['./article-details.component.scss']
})
export class ArticleDetailsComponent implements OnInit, OnDestroy {

  public uiElements: UiElementsDto[];
  private uiElementsSubscription: Subscription;
  public article: ArticleDTO;
  private currentLanguage: CurrentLanguageDTO;
  private currentLanguageSubscription$: Subscription;
  private loggedUserSubscription: Subscription;
  public loggedUserData: UserDTO;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly articleService: ArticlesService,
    private readonly languageService: LanguagesService,
    private readonly authService: AuthService,
    private readonly translationService: TranslationsService,
    private readonly notification: NotificationService,
    private readonly uiElementsService: UiElementsService,
    private readonly UIelFinder: UIelFinderPipe,
  ) { }

  ngOnInit() {
    this.route.data.subscribe(
      ({ article }) => {
        this.article = article;
      },
    );
    const id = this.route.snapshot.params.id;
    this.currentLanguageSubscription$ = this.languageService.currentLanguageCode$
      .subscribe((language: CurrentLanguageDTO) => {
        this.currentLanguage = language;
        this.articleService.getArticleById(this.currentLanguage.code, id)
            .subscribe(
              (data) => {
                this.article = data;
              },
              (error) => this.notification.error(error.error.error),
            );
      });
    this.loggedUserSubscription = this.authService.loggedUserData$.subscribe(
      (data: UserDTO) => this.loggedUserData = data,
    );
    this.uiElementsSubscription = this.uiElementsService.uiElementsData$.subscribe((data) => {
      this.uiElements = data;
    });
  }

  public editArticle(editedArticle: ArticleDTO) {
    if (this.article.versions[0].languageCode === this.currentLanguage.code) {
      this.article = editedArticle;
    } else {
      this.articleService.getArticleById(this.currentLanguage.code, this.article.id)
        .subscribe(
          (data) => this.article = data,
          (error) => this.notification.error(error.error.error),
        );
    }
  }

  public rate(rating: number, ratingId: string) {
    this.translationService.rateTranslation(ratingId, rating)
      .subscribe(
        (ratingData: RatingDTO) => {
          (this.article.versions[0].title as ArticleTranslationDTO).rating.id === ratingId ?
            (this.article.versions[0].title as ArticleTranslationDTO).rating = ratingData :
            (this.article.versions[0].content as ArticleTranslationDTO).rating = ratingData;
          this.notification.success(this.UIelFinder.transform(this.uiElements, 'translation_rated'));
        },
        (error) => this.notification.error(error.error.error),
      );
  }

  ngOnDestroy() {
    this.currentLanguageSubscription$.unsubscribe();
    this.loggedUserSubscription.unsubscribe();
    this.uiElementsSubscription.unsubscribe();
  }

}
