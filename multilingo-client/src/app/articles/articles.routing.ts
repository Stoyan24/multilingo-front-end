import { AdminGuard } from './../core/guards/admin.guard';
import { ArticlesResolverService } from './../core/services/articles-resolver.service';
import { AllArticlesComponent } from './all-articles/all-articles.component';
import { AllVersionsComponent } from './../versions/all-versions/all-versions.component';
import { ArticleDetailsComponent } from './article-details/article-details.component';
import { ArticleResolverService } from './services/article-resolver.service';
import { ContributorGuard } from './../core/guards/contributor.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VersionsResolverService } from '../versions/services/versions-resolver.service';

const routes: Routes = [
  { path: '',
    component: AllArticlesComponent,
    resolve: { articles: ArticlesResolverService },
    canActivate: [AdminGuard],
   },
  { path: ':id/versions',
    component: AllVersionsComponent,
    resolve: { versions: VersionsResolverService },
    canActivate: [ ContributorGuard ],
  },
  { path: ':id',
    component: ArticleDetailsComponent,
    resolve: { article: ArticleResolverService },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArticlesRoutingModule {}
