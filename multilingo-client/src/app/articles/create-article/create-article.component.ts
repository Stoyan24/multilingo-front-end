import { NotificationService } from '../../core/services/notification.service';
import { LanguagesService } from '../../core/services/languages.service';
import { CreateArticleDTO } from '../models/create-article.dto';
import { CollapseComponent } from 'angular-bootstrap-md';
import { Component, AfterViewInit, ViewChildren, OnInit, Output, EventEmitter, Input} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LanguageDto } from 'src/app/shared/languages/models/language.dto';
import { UiElementsDto} from '../../shared/ui-elements/models/ui-elements.dto';
@Component({
  selector: 'app-create-article',
  templateUrl: './create-article.component.html',
  styleUrls: ['./create-article.component.scss']
})
export class CreateArticleComponent implements OnInit, AfterViewInit {

  @Input() public uiElements: UiElementsDto[];
  @ViewChildren(CollapseComponent) collapses: CollapseComponent[];
  @Output() public oNCreate: EventEmitter<CreateArticleDTO> = new EventEmitter<CreateArticleDTO>();
  public createArticleForm: FormGroup;
  public collapsed = true;
  public language: string;

  constructor(
    public readonly formBuilder: FormBuilder,
    public readonly notification: NotificationService,
    public readonly languageService: LanguagesService,
  ) { }

  ngOnInit() {
    this.createArticleForm = this.formBuilder.group({
      imgPath: [
        '',
        [Validators.required],
      ],
      title: [
        '',
        [Validators.required],
      ],
      content: ['', [Validators.required]],
    });
  }

  ngAfterViewInit() {
    Promise.resolve().then(() => {
      this.collapses.forEach((collapse: CollapseComponent) => {
        collapse.toggle();
      });
    });
  }

  public get imgPath() { return this.createArticleForm.get('imgPath'); }
  public get title() { return this.createArticleForm.get('title'); }
  public get content() { return this.createArticleForm.get('content'); }

  private toggle() {
    this.collapsed = !this.collapsed;
  }

  public setArticleLanguage(language: LanguageDto) {
    this.language = language.code;
  }

  public createArticle(article: CreateArticleDTO) {
    const articleToCreate = {...article, languageCode: this.language};
    this.oNCreate.emit(articleToCreate);
    this.createArticleForm.reset();
  }
}
