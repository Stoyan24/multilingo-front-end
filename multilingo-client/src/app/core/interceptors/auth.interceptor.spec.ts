import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import {AuthInterceptor} from './auth.interceptor';
import {CONFIG} from '../../config/config';
import {TranslationsService} from '../services/translations.service';

describe(`AuthHttpInterceptor`, () => {
  let service: TranslationsService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        TranslationsService,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: AuthInterceptor,
          multi: true,
        },
      ],
    });

    service = TestBed.get(TranslationsService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should add an Authorization header', () => {
    service.getAllTranslations(5, 5).subscribe(response => {
      expect(response).toBeTruthy();
    });
  });
  it('should se if Authorization header is correct', () => {
    service.getAllTranslations(5, 5).subscribe();

    const httpRequest = httpMock.expectOne(`${CONFIG.API_DOMAIN_NAME}/translations?take=5&skip=5`);

    expect(httpRequest.request.headers.has('Authorization')).toEqual(true);
  });
});
