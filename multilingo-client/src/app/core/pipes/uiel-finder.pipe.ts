import { Pipe, PipeTransform } from '@angular/core';
import { UiElementsDto } from '../../shared/ui-elements/models/ui-elements.dto';

@Pipe({
  name: 'UIelFinder'
})
export class UIelFinderPipe implements PipeTransform {

  transform(value: UiElementsDto[], ...args: any[]): any {
    if (value === undefined) {
      return '...';
    }
    const text = value.find((el: UiElementsDto) => el.resourceName === args[0]);
    if (!text) {
      return '...';
    }
    return text.content;
  }

}
