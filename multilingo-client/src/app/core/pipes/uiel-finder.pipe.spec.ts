import { UiElementsDto } from '../../shared/ui-elements/models/ui-elements.dto';
import {UIelFinderPipe} from './uiel-finder.pipe';

describe('UIelFinderPipe', () => {

  let pipe: UIelFinderPipe;
  const mock: UiElementsDto[] = [
    {
      content: 'Translation rated',
      resourceName: 'translation_rated',
      languageCode: 'en',
    },
    {
      content: 'You do not have any articles yet!',
      resourceName: 'no_articles',
      languageCode: 'en',
    },
    {
      content: 'Unauthorized access!',
      resourceName: 'un_access',
      languageCode: 'en',
    },
  ];
  beforeEach(() => {
    pipe = new UIelFinderPipe();
  });

  it('create an instance', () => {
    pipe = new UIelFinderPipe();
    expect(pipe).toBeTruthy();
  });

  it('if find ui-element in array should return it', () => {
    const result = pipe.transform(mock, 'no_articles');
    expect(result).toEqual('You do not have any articles yet!');
  });
  it('if not find ui-element in array should return undefined', () => {
    expect(pipe.transform(mock, 'XXX')).toEqual('...');
  });
  it('if ui-elements value is undefined to return ...', () => {
    expect(pipe.transform(undefined, 'XXX')).toEqual('...');
  });
});
