import { GetRolePipe } from './get-role.pipe';

describe('GetRolePipe', () => {

  let pipe: GetRolePipe;
  const mock = {
    roles: ['Editor', 'Contributor']
  };
  beforeEach(() => {
    pipe = new GetRolePipe();
  });

  it('create an instance', () => {
    pipe = new GetRolePipe();
    expect(pipe).toBeTruthy();
  });
  it('if array have role to return true', () => {
    const result = pipe.transform(mock, 'Editor');
    expect(result).toBe(true);
  });
  it('if array dont have role to return false', () => {
    const result = pipe.transform(mock, 'Administrator');
    expect(result).toBe(false);
  });
});
