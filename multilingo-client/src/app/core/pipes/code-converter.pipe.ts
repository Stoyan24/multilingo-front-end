import { Pipe, PipeTransform } from '@angular/core';
import { languages } from '../../config/languages-code';

@Pipe({
  name: 'CodeConverterPipe'
})
export class CodeConverterPipe implements PipeTransform {

  transform(value: any, ...args: any[]): boolean {
      let transformed = value;
      languages.forEach(el => {
        if (el.code === value) {
          transformed = el.name;
        }
      });
      return transformed;
  }
}
