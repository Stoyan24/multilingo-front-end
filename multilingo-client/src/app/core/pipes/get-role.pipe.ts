import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'getRole'
})
export class GetRolePipe implements PipeTransform {

  transform(value: any, ...args: any[]): boolean {
    return ((value !== null) && (value.roles !== undefined)) ? value.roles.includes(args[0]) : false;
  }

}
