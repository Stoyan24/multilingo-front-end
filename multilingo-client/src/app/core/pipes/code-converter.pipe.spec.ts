import {CodeConverterPipe} from './code-converter.pipe';

describe('CodeConverterPipe', () => {

  let pipe: CodeConverterPipe;
  beforeEach(() => {
    pipe = new CodeConverterPipe();
  });

  it('create an instance', () => {
    pipe = new CodeConverterPipe();
    expect(pipe).toBeTruthy();
  });
  it('should return language name', () => {
    const result = pipe.transform('ru');
    expect(result).toEqual('Russian');
  });
});
