import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { NotificationService } from '../services/notification.service';

@Injectable()
export class AdminGuard implements CanActivate {

  public constructor(
    private readonly authService: AuthService,
    private readonly notificationService: NotificationService,
    private readonly router: Router,
  ) {}

  public canActivate(): boolean {
    const user = this.authService.getUserDataIfAuthenticated();
    if (user === null) {
      return false;
    }
    if (!user.roles.includes('Administrator')) {
      this.notificationService.error('Unauthorized access!');
      this.router.navigate(['home']);
      return false;
    }
    return true;
  }
}
