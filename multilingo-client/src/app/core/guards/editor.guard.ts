import { NotificationService } from 'src/app/core/services/notification.service';
import { AuthService } from 'src/app/core/services/auth.service';
import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { UIelFinderPipe } from '../pipes/uiel-finder.pipe';
import { UiElementsService } from '../services/ui-elements.service';

@Injectable()
export class EditorGuard implements CanActivate {

  public constructor(
    private readonly authService: AuthService,
    private readonly notificationService: NotificationService,
    private readonly router: Router,
  ) {}

  public canActivate(): boolean {
    const user = this.authService.getUserDataIfAuthenticated();
    if (user === null) {
      return false;
    }
    if (!user.roles.includes('Editor')) {
      this.notificationService.error('Unauthorized access!');
      this.router.navigate(['home']);
      return false;
    }
    return true;
  }
}
