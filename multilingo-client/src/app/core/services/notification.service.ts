import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private notificationToast: ToastrService) {}

  public success(message: string, title?: string): void {
    this.notificationToast.success(message);
  }

  public error(message: string, title?: string): void {
    this.notificationToast.warning(message);
  }

  public warning(message: string, title?: string): void {
    this.notificationToast.info(message);
  }

  public info(message: string, title?: string): void {
    this.notificationToast.error(message);
  }
}
