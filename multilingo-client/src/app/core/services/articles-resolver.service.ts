import { CurrentLanguageDTO } from './../../shared/languages/models/current-language.dto';
import { ArticleDTO } from '../../articles/models/articles.dto';
import { ArticlesService } from './articles.service';
import { NotificationService } from './notification.service';
import { Injectable } from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { LanguagesService } from './languages.service';

@Injectable()
export class ArticlesResolverService implements Resolve<ArticleDTO[]> {

  constructor(
    private readonly router: Router,
    private readonly articleService: ArticlesService,
    public readonly notification: NotificationService,
    public readonly languageService: LanguagesService,
  ) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<ArticleDTO[]> {

    const sysLanguage = window.navigator.language.slice(0, 2);
    let languageCode: string;
    this.languageService.currentLanguageCode$
      .subscribe(
        (language: CurrentLanguageDTO) => languageCode = language.code,
        (error) => this.notification.error(error.error.error),
      ).unsubscribe();

    return this.articleService.getAllArticles(sysLanguage)
      .pipe(
        map(articles => {
          if (articles) {
            return articles;
          } else {
            this.router.navigate(['/not-found']);
            this.notification.error(`There was an unexpected error.`);
            return [];
          }
        })
      );
  }

}
