import { StorageService } from './storage.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { JwtHelperService} from '@auth0/angular-jwt';
import { CONFIG } from '../../config/config';
import { RegisterUserDto} from '../../components/register/models/register-user.dto';
import { LoginUserDto } from '../../components/login/models/login-user.dto';
import { LanguagesService } from './languages.service';
import { UserDTO } from '../../users/models/user.dto';
import { UiElementsService } from './ui-elements.service';

@Injectable()
export class AuthService {

  private loggedUserDataSubject$ = new BehaviorSubject<any>(
    this.getUserDataIfAuthenticated()
  );

  public constructor(
    private readonly http: HttpClient,
    private readonly storage: StorageService,
    private readonly jwtService: JwtHelperService,
    private readonly languageService: LanguagesService,
    private readonly uiService: UiElementsService,
  ) { }

  public get loggedUserData$(): Observable<any> {
    return this.loggedUserDataSubject$.asObservable();
  }

  public emitUserData(user: any): void {
    this.loggedUserDataSubject$.next(user);
  }

  public getUserDataIfAuthenticated(): any {
    const token: string = this.storage.getItem('token');

    if (token && this.jwtService.isTokenExpired(token)) {
      this.storage.removeItem('token');
      return null;
    }

    return token ? this.jwtService.decodeToken(token) : null;
  }

  public register(user: RegisterUserDto): Observable<any> {
    return this.http.post<any>(
      `${CONFIG.API_DOMAIN_NAME}/users`,
      user
    );
  }

   public signIn(user: LoginUserDto): Observable<any> {
    return this.http
      .post<any>(`${CONFIG.API_DOMAIN_NAME}/session`, user)
      .pipe(
        tap(({ token }) => {
          this.storage.setItem('token', token);
          const userData: UserDTO = this.jwtService.decodeToken(token);
          this.emitUserData(userData);
          this.uiService.getAllUIElements(userData.preferedLangCode.code).subscribe((data) => {
            this.uiService.emitUIELementsData$(data);
          });
          this.languageService.changeCurrentLanguageCode(userData.preferedLangCode);
        })
      );
  }

  public signOut(): Observable<any> {
    return this.http.delete(`${CONFIG.API_DOMAIN_NAME}/session`)
      .pipe(
        tap(
          () => {
            this.storage.removeItem('token');
            this.emitUserData(null);
          })
      );
  }
}
