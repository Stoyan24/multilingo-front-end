import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CONFIG} from '../../config/config';
import {ShowUserDto} from '../../admin-panel/models/show-user.dto';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private readonly http: HttpClient) {
  }

  public getAllUsers(): Observable<ShowUserDto[]> {
    return this.http.get<ShowUserDto[]>(`${CONFIG.API_DOMAIN_NAME}/users`);
  }

  public addRole(id: string, role: string): Observable<ShowUserDto> {
    return this.http.put<ShowUserDto>(`${CONFIG.API_DOMAIN_NAME}/users/${id}/role`, {name: role});
  }

  public deleteUser(id: string): Observable<ShowUserDto> {
    return this.http.delete<ShowUserDto>(`${CONFIG.API_DOMAIN_NAME}/users/${id}`);
  }
}
