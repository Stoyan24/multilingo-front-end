import { VersionDTO } from '../../versions/models/version.dto';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CONFIG } from '../../config/config';

@Injectable()
export class VersionsService {

  constructor(
    private readonly http: HttpClient,
  ) { }

  public getAllVersions(articleId: string): Observable<VersionDTO[]> {
    return this.http.get<VersionDTO[]>(`${CONFIG.API_DOMAIN_NAME}/articles/${articleId}/versions`);
  }

  public setCurrentVersion(articleId: string, versionId: string): Observable<VersionDTO[]> {
    return this.http.put<VersionDTO[]>(`${CONFIG.API_DOMAIN_NAME}/articles/${articleId}/versions/${versionId}`, undefined);
  }

}
