import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { CONFIG } from '../../config/config';
import { UiElementsDto } from '../../shared/ui-elements/models/ui-elements.dto';
import { LanguagesService } from './languages.service';
import { LanguageDto } from '../../shared/languages/models/language.dto';

@Injectable({
  providedIn: 'root'
})
export class UiElementsService {

  public uiElements: UiElementsDto[];

  private uiElements$ = new BehaviorSubject<UiElementsDto[]>(this.uiElements);
  private browserLanguage: string = window.navigator.language.slice(0, 2);


  constructor(
    private readonly http: HttpClient,
    private readonly languageService: LanguagesService,
  ) {
    this.languageService.languagesData$.subscribe((data: LanguageDto[] = []) => {
      if (data.some((l: LanguageDto) => l.code === this.browserLanguage)) {
        this.getAllUIElements(window.navigator.language.slice(0, 2)).subscribe((ui: UiElementsDto[]) => {
          this.uiElements$.next(ui);
        });
      } else {
        this.getAllUIElements('en').subscribe((ui: UiElementsDto[]) => {
          this.uiElements$.next(ui);
        });
      }
    });
  }

  public get uiElementsData$(): Observable<UiElementsDto[]> {
    return this.uiElements$.asObservable();
  }

  public emitUIELementsData$(value: UiElementsDto[]) {
    this.uiElements$.next(value);
  }

  public getAllUIElements(languageCode): Observable<UiElementsDto[]> {
    return this.http.get<UiElementsDto[]>(`${CONFIG.API_DOMAIN_NAME}/ui-component`,
      {params: new HttpParams().set('languageCode', languageCode)},
      );
  }
}
