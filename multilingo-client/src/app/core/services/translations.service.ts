import {RatingDTO} from './../../shared/rating/models/rating.dto';
import {TranslationDTO} from './../../translations/model/translation.dto';
import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CONFIG} from '../../config/config';

@Injectable({
  providedIn: 'root'
})
export class TranslationsService {

  constructor(private readonly http: HttpClient) {
  }

  public getAllTranslations(skip, take): Observable<TranslationDTO[]> {
    return this.http.get<TranslationDTO[]>(`${CONFIG.API_DOMAIN_NAME}/translations`,
      {params: new HttpParams().set('take', skip).set('skip', take)},
    );
  }

  public getTranslationByContent(content: string): Observable<TranslationDTO[]> {
    return this.http.get<TranslationDTO[]>(`${CONFIG.API_DOMAIN_NAME}/translations/${content}`);
  }


  public editTranslation(id: string, translation: string): Observable<TranslationDTO> {
    return this.http.put<TranslationDTO>(`${CONFIG.API_DOMAIN_NAME}/translations/${id}`, translation);
  }

  public rateTranslation(id: string, rating: number): Observable<RatingDTO> {
    return this.http.put<RatingDTO>(`${CONFIG.API_DOMAIN_NAME}/translations/${id}/rate`, {rating});
  }
}
