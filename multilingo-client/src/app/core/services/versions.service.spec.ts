import {TestBed, getTestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {VersionsService} from './versions.service';
import {CONFIG} from '../../config/config';
import {VersionDTO} from '../../versions/models/version.dto';


describe('StudentsService', () => {
  let injector: TestBed;
  let service: VersionsService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [VersionsService],
    });

    injector = getTestBed();
    service = injector.get(VersionsService);
    httpMock = injector.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  describe('VersionsService test', () => {
    const dummyResponse: VersionDTO[] = [
      {
        id: 'test',
        imgPath: 'test',
        title: 'test',
        content: 'test',
        version: 5,
        languageCode: 'bg',
        isCurrent: true,
        isDeleted: false
      }
    ];

    it('getAllVersions() should return data', () => {
      const aID = '12345';
      service.getAllVersions(aID).subscribe((res) => {
        expect(res).toEqual(dummyResponse);
      });

      const req = httpMock.expectOne(`${CONFIG.API_DOMAIN_NAME}/articles/${aID}/versions`);
      expect(req.request.method).toBe('GET');
      req.flush(dummyResponse);
    });

    it('setCurrentVersion() should return data', () => {
      const aID = '12345';
      const vID = '0000';
      service.setCurrentVersion(aID, vID).subscribe((res) => {
        expect(res).toEqual(dummyResponse);
      });

      const req = httpMock.expectOne(`${CONFIG.API_DOMAIN_NAME}/articles/${aID}/versions/${vID}`);
      expect(req.request.method).toBe('PUT');
      req.flush(dummyResponse);
    });
  });
});
