import {CurrentLanguageDTO} from './../../shared/languages/models/current-language.dto';
import {LanguageDto} from './../../shared/languages/models/language.dto';
import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable, BehaviorSubject} from 'rxjs';
import {CONFIG} from '../../config/config';
import {CreateLanguageDto} from '../../shared/languages/models/create-language.dto';

@Injectable({
  providedIn: 'root'
})
export class LanguagesService {

  public languages: LanguageDto[];

  private languageCode$ = new BehaviorSubject<CurrentLanguageDTO>({
    code: window.navigator.language.slice(0, 2),
    name: '',
  });

  private allLanguage$ = new BehaviorSubject<LanguageDto[]>(this.languages);


  public currentLanguageCode$ = this.languageCode$.asObservable();

  constructor(private readonly http: HttpClient) {
    this.getAllLanguages().subscribe((data) => {
      this.allLanguage$.next(data);
    });
  }

  public changeCurrentLanguageCode(language: CurrentLanguageDTO) {
    this.languageCode$.next(language);
  }

  public get languagesData$(): Observable<LanguageDto[]> {
    return this.allLanguage$.asObservable();
  }

  public emitLanguageData$(value: LanguageDto[]) {
     this.allLanguage$.next(value);
  }

  public detectArticleLanguage(text: string): Observable<string> {
    return this.http.get<string>(
      `${CONFIG.API_DOMAIN_NAME}/languages`,
      {params: new HttpParams().set('text', text)},
    );
  }

  public getAllLanguages(): Observable<LanguageDto[]> {
    return this.http.get<LanguageDto[]>(`${CONFIG.API_DOMAIN_NAME}/languages`);
  }

  public addLanguage(body: CreateLanguageDto): Observable<LanguageDto[]> {
    return this.http.post<LanguageDto[]>(`${CONFIG.API_DOMAIN_NAME}/languages`, {name: body.name, code: body.code});
  }
}
