import { EditArticleDTO } from './../../articles/models/edit-article';
import { ArticleDTO } from './../../articles/models/articles.dto';
import { CreateArticleDTO } from './../../articles/models/create-article.dto';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CONFIG } from 'src/app/config/config';
import { VersionDTO } from 'src/app/versions/models/version.dto';

@Injectable({
  providedIn: 'root'
})
export class ArticlesService {

  constructor(
    private readonly http: HttpClient,
    ) { }

    public createArticle(content: CreateArticleDTO): Observable<ArticleDTO> {
      return this.http.post<ArticleDTO>(`${CONFIG.API_DOMAIN_NAME}/articles`, content );
    }

    public editArticle(content: EditArticleDTO, articleId: string): Observable<ArticleDTO> {
      return this.http.post<ArticleDTO>(`${CONFIG.API_DOMAIN_NAME}/articles/${articleId}/versions`, content );
    }

    public deleteArticle(articleId: string): Observable<ArticleDTO> {
      return this.http.delete<ArticleDTO>(`${CONFIG.API_DOMAIN_NAME}/articles/${articleId}`);
    }

    public getAllArticles(languageCode: string): Observable<ArticleDTO[]> {
      return this.http.get<ArticleDTO[]>(
        `${CONFIG.API_DOMAIN_NAME}/articles`,
        { params: new HttpParams().set('language', languageCode) },
      );
    }

    public getArticleById(languageCode: string, articleId: string): Observable<ArticleDTO> {
      return this.http.get<ArticleDTO>(
        `${CONFIG.API_DOMAIN_NAME}/articles/${articleId}`,
        { params: new HttpParams().set('language', languageCode) },
      );
    }

}
