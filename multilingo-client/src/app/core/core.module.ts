import { ContributorGuard } from './guards/contributor.guard';
import { ArticlesResolverService } from './services/articles-resolver.service';
import { NgModule, Optional, SkipSelf} from '@angular/core';
import { StorageService} from './services/storage.service';
import { NotificationService} from './services/notification.service';
import { ToastrService} from 'ngx-toastr';
import { TranslationsService} from './services/translations.service';
import { CodeConverterPipe} from './pipes/code-converter.pipe';
import { ArticlesService } from './services/articles.service';
import { UsersService} from './services/user.service';
import { GetRolePipe } from './pipes/get-role.pipe';
import { AdminGuard } from './guards/admin.guard';
import { EditorGuard } from './guards/editor.guard';
import { UIelFinderPipe } from './pipes/uiel-finder.pipe';
import { VersionsService } from './services/versions.service';


@NgModule({
  providers: [
    AdminGuard,
    EditorGuard,
    ContributorGuard,
    StorageService,
    NotificationService,
    ToastrService,
    TranslationsService,
    ToastrService,
    ArticlesService,
    ArticlesResolverService,
    UsersService,
    VersionsService,
    UIelFinderPipe,
  ],
  declarations: [CodeConverterPipe, GetRolePipe, UIelFinderPipe],
  exports: [CodeConverterPipe, GetRolePipe, UIelFinderPipe]
})
export class CoreModule {
  public constructor(@Optional() @SkipSelf() parent: CoreModule) {
    if (parent) {
      return parent;
    }
  }
}
