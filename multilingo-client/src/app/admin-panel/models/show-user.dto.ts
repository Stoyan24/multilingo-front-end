import {RoleDTO} from '../../users/models/role.dto';

export class ShowUserDto {
  public id: string;
  public username: string;
  public email: string;
  public avatarPath: string;
  public preferedLangCode: string;
  public roles: RoleDTO[];
}
