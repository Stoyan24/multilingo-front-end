import {Component, OnDestroy, OnInit} from '@angular/core';
import {ShowUserDto} from '../models/show-user.dto';
import {UsersService} from '../../core/services/user.service';
import {NotificationService} from '../../core/services/notification.service';
import {LanguageDto} from '../../shared/languages/models/language.dto';
import {LanguagesService} from '../../core/services/languages.service';
import {CreateLanguageDto} from '../../shared/languages/models/create-language.dto';
import {Subscription} from 'rxjs';
import {UiElementsDto} from '../../shared/ui-elements/models/ui-elements.dto';
import {UiElementsService} from '../../core/services/ui-elements.service';
import {UIelFinderPipe} from '../../core/pipes/uiel-finder.pipe';

@Component({
  selector: 'app-admin-panel-view',
  templateUrl: './admin-panel-view.component.html',
  styleUrls: ['./admin-panel-view.component.scss']
})
export class AdminPanelViewComponent implements OnInit, OnDestroy {

  public users: ShowUserDto[];
  public languages: LanguageDto[];
  private languagesSubscription: Subscription;
  public uiElements: UiElementsDto[];
  private uiElementsSubscription: Subscription;


  constructor(
    private readonly usersService: UsersService,
    private readonly notification: NotificationService,
    private readonly languageService: LanguagesService,
    private readonly uiElementsService: UiElementsService,
    private readonly UIelFinder: UIelFinderPipe,
  ) {
  }

  ngOnInit() {
    this.usersService.getAllUsers()
      .subscribe((data: ShowUserDto[]) => {
          this.users = data;
        },
        (error) => this.notification.error(error.error.error));
    this.languagesSubscription = this.languageService.languagesData$.subscribe((data) => {
      this.languages = data;
    });
    this.uiElementsSubscription = this.uiElementsService.uiElementsData$.subscribe((data) => {
      this.uiElements = data;
    });
  }

  public deleteUser(id) {
    this.usersService.deleteUser(id)
      .subscribe((data: ShowUserDto) => {
          this.users = this.users.filter((u: ShowUserDto) => u.id !== data.id);
          this.notification.success(this.UIelFinder.transform(this.uiElements, 'user_deleted'));
        },
        (error) => this.notification.error(error.error));
  }

  public addLanguage(value: CreateLanguageDto) {
    this.languageService.addLanguage(value)
      .subscribe((data: LanguageDto[]) => {
          this.languages = data;
          this.notification.success(this.UIelFinder.transform(this.uiElements, 'user_added'));
          this.languageService.emitLanguageData$(data);
        }
        , (err) => this.notification.error(err.error.error));
  }

  public ngOnDestroy(): void {
    this.languagesSubscription.unsubscribe();
    this.uiElementsSubscription.unsubscribe();
  }
}
