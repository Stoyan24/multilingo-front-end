import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ShowUserDto} from '../models/show-user.dto';
import {UsersService} from '../../core/services/user.service';
import {NotificationService} from '../../core/services/notification.service';
import {UiElementsDto} from '../../shared/ui-elements/models/ui-elements.dto';
import {UIelFinderPipe} from '../../core/pipes/uiel-finder.pipe';

@Component({
  selector: 'app-user-view',
  templateUrl: './user-view.component.html',
  styleUrls: ['./user-view.component.scss']
})
export class UserViewComponent implements OnInit {

  @Input() public user: ShowUserDto;
  @Input() uiElements: UiElementsDto[];
  @Output() public readonly userInfo: EventEmitter<string> = new EventEmitter<string>();


  constructor(
    private readonly usersService: UsersService,
    private readonly notification: NotificationService,
    private readonly UIelFinder: UIelFinderPipe,
  ) {
  }
  ngOnInit() {
  }

  public changeRoles(value) {
    this.usersService.addRole(this.user.id, value).subscribe((data: ShowUserDto) => {
      this.user.roles = data.roles;
      this.notification.success(this.UIelFinder.transform(this.uiElements, 'role_change'));
    },
      (error) => this.notification.error(error.error.error));
  }

  public deleteUser() {
    this.userInfo.emit(this.user.id);
  }

}
