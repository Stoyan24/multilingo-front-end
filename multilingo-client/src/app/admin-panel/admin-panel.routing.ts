import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminPanelViewComponent } from './admin-panel-view/admin-panel-view.component';
import { AdminGuard } from '../core/guards/admin.guard';



const routes: Routes = [
  { path: '', component: AdminPanelViewComponent, canActivate: [AdminGuard], pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminPanelRouting {}
