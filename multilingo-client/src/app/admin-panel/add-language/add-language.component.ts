import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CreateLanguageDto} from '../../shared/languages/models/create-language.dto';
import {UiElementsDto} from '../../shared/ui-elements/models/ui-elements.dto';
import {UiElementsService} from '../../core/services/ui-elements.service';

@Component({
  selector: 'app-add-language',
  templateUrl: './add-language.component.html',
  styleUrls: ['./add-language.component.scss']
})
export class AddLanguageComponent implements OnInit {

  @Output() public languageEmit: EventEmitter<CreateLanguageDto> = new EventEmitter<CreateLanguageDto>();
  @Input() uiElements: UiElementsDto[];

  public newLanguage: FormGroup;

  constructor(
    private readonly formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.newLanguage = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(20)]],
      code: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(2)]],
    });
  }

  get name() {
    return this.newLanguage.get('name');
  }

  get code() {
    return this.newLanguage.get('code');
  }


  public addLanguage() {
    this.languageEmit.emit(this.newLanguage.value);
  }
}
