import {Component, Input, OnInit} from '@angular/core';
import {LanguageDto} from '../../shared/languages/models/language.dto';
import {UiElementsDto} from '../../shared/ui-elements/models/ui-elements.dto';

@Component({
  selector: 'app-languages-view',
  templateUrl: './languages-view.component.html',
  styleUrls: ['./languages-view.component.scss']
})
export class LanguagesViewComponent implements OnInit {

  @Input() languages: LanguageDto[];
  @Input() uiElements: UiElementsDto[];


  constructor() { }

  ngOnInit() {}

}
