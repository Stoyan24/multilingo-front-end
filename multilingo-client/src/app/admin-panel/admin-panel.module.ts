import { NgModule } from '@angular/core';
import { SharedModule} from '../shared/shared.module';
import { CoreModule} from '../core/core.module';
import { AdminPanelRouting} from './admin-panel.routing';
import { AdminPanelViewComponent } from './admin-panel-view/admin-panel-view.component';
import { UserViewComponent } from './user-view/user-view.component';
import { LanguagesViewComponent } from './languages-view/languages-view.component';
import { AddLanguageComponent } from './add-language/add-language.component';
import { NgxSpinnerModule } from 'ngx-spinner';



@NgModule({
  declarations: [AdminPanelViewComponent, UserViewComponent, LanguagesViewComponent, AddLanguageComponent],
  imports: [
    SharedModule,
    CoreModule,
    AdminPanelRouting,
    NgxSpinnerModule,
  ],
  exports: [AdminPanelViewComponent, UserViewComponent, LanguagesViewComponent, AddLanguageComponent]
})
export class AdminPanelModule { }
