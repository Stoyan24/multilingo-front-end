import { Component, Input, Output, EventEmitter } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UiElementsDto } from '../ui-elements/models/ui-elements.dto';

@Component({
  selector: 'app-diolog-modal',
  templateUrl: './diolog-modal.component.html',
  styleUrls: ['./diolog-modal.component.scss']
})
export class DiologModalComponent {

  @Input() public uiElements: UiElementsDto[];
  @Input() public message: string;
  @Input() public btnIcon: string;
  @Input() public btnClass: string;
  @Output() public oNArticleDelete: EventEmitter<boolean> = new EventEmitter<boolean>();
  public closeResult = false;

  public constructor(public modal: NgbModal) {}

  open(content) {
    this.modal.open(content, { ariaLabelledBy: 'modal-title' }).result.then((result) => {
      this.oNArticleDelete.emit(result);
    }, () => {
      this.oNArticleDelete.emit(false);
    });
  }

}
