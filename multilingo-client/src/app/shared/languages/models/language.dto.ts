export class LanguageDto {

  public id?: string;
  public name: string;
  public code: string;
  public isActive?: boolean;

}
