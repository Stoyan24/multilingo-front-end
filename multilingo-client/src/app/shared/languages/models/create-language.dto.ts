export class CreateLanguageDto {
  public name: string;
  public code: string;
}
