import { DropdownMenuComponent } from './dropdown/dropdown.component';
import { PaginationComponent } from './pagination/pagination.component';
import { FormDropdownComponent } from './form-dropdown/form-dropdown.component';

import { MdbModule } from './mdb/mdb.module';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DiologModalComponent } from './diolog-modal/diolog-modal.component';
import { CoreModule } from '../core/core.module';
import { SearchBarComponent } from './search-bar/search-bar.component';

@NgModule({
  declarations: [
    DropdownMenuComponent,
    PaginationComponent,
    FormDropdownComponent,
    DiologModalComponent,
    SearchBarComponent,
  ],
  imports: [
    RouterModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MdbModule,
    NgbModule,
    CoreModule,
  ],
  exports: [
    RouterModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FormDropdownComponent,
    DropdownMenuComponent,
    DiologModalComponent,
    SearchBarComponent,
    MdbModule,
    NgbModule,
  ],
})
export class SharedModule { }
