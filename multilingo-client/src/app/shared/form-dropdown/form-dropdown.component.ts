import { NotificationService } from './../../core/services/notification.service';
import { LanguagesService } from './../../core/services/languages.service';
import {Component, OnInit, Output, EventEmitter, Input} from '@angular/core';
import { LanguageDto } from '../languages/models/language.dto';
import { UiElementsDto } from '../ui-elements/models/ui-elements.dto';

@Component({
  selector: 'app-form-dropdown',
  templateUrl: './form-dropdown.component.html',
  styleUrls: ['./form-dropdown.component.scss']
})
export class FormDropdownComponent implements OnInit {


  public languages: LanguageDto[];
  @Input() chosenLanguage: string;
  @Output() public oNLanguageChange: EventEmitter<LanguageDto> = new EventEmitter<LanguageDto>();

  constructor(
    private readonly languageService: LanguagesService,
    private readonly notification: NotificationService,
      ) {}

  ngOnInit() {
    this.languageService.getAllLanguages().subscribe((languages: LanguageDto[]) => {
      this.languages = languages;
    },
    (err) => this.notification.error(err));
  }

  public emitLanguage(language: LanguageDto) {
    this.chosenLanguage = language.name;
    this.oNLanguageChange.emit(language);
  }

}
