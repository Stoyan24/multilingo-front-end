export class UiElementsDto {
  public id?: string;
  public content: string;
  public resourceName: string;
  public isDeleted?: boolean;
  public languageCode: string;
}
