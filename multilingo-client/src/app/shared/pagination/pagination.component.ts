import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html'
})
export class PaginationComponent {

  public page: number;
  @Output() public oNPageEmit: EventEmitter<number> = new EventEmitter<number>();

  public emitPage() {
    this.oNPageEmit.emit(this.page);
  }
}
