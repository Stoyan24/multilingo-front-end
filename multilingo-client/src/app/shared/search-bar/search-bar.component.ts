import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-serach-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {

  @Input() search: string;
  @Output() public readonly termEmit: EventEmitter<string> = new EventEmitter<string>();
  public term: string;

  constructor() { }

  ngOnInit() {
  }

  public emit() {
    this.termEmit.emit(this.term);
  }

}
