import { LanguageDto } from './../languages/models/language.dto';
import { CurrentLanguageDTO } from './../languages/models/current-language.dto';
import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { LanguagesService } from '../../core/services/languages.service';
import { NotificationService } from '../../core/services/notification.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-dropdown-menu',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss']
})
export class DropdownMenuComponent implements OnInit, OnDestroy {

  public languages: LanguageDto[];
  public currentLanguage: CurrentLanguageDTO;
  private currentLanguageSubscription: Subscription;
  private languageSubscription: Subscription;
  @Output() public oNLanguageSelect: EventEmitter<LanguageDto> = new EventEmitter<LanguageDto>();

  constructor(
    private readonly languageService: LanguagesService,
    private readonly notification: NotificationService,
  ) { }

  ngOnInit() {
    this.languageSubscription = this.languageService.languagesData$
      .subscribe((languages: LanguageDto[]) => {
        this.languages = languages;
        this.languageService.currentLanguageCode$
          .subscribe((language: CurrentLanguageDTO) => {
            this.currentLanguage = language;
            const foundLanguage = languages
              .find((lang: LanguageDto) => lang.code === this.currentLanguage.code);
            if (foundLanguage === undefined) {
              this.currentLanguage.code = 'en';
              this.currentLanguage.name = 'English';
            } else {
              this.currentLanguage.name = foundLanguage.name;
            }
          },
            (error) => this.notification.error(error.error.error));
      },
        (error) => this.notification.error(error.error.error));
  }

  public emitLanguage(language: LanguageDto) {
    this.oNLanguageSelect.emit(language);
  }

  ngOnDestroy() {
    this.currentLanguageSubscription.unsubscribe();
    this.languageSubscription.unsubscribe();
  }

}
