import {Component, OnDestroy, OnInit} from '@angular/core';
import {TranslationsService} from '../../core/services/translations.service';
import {TranslationDTO} from '../model/translation.dto';
import {UiElementsDto} from '../../shared/ui-elements/models/ui-elements.dto';
import {Subscription} from 'rxjs';
import {UiElementsService} from '../../core/services/ui-elements.service';

@Component({
  selector: 'app-all-translations',
  templateUrl: './all-translations.component.html',
  styleUrls: ['./all-translations.component.scss']
})
export class AllTranslationsComponent implements OnInit, OnDestroy {

  public skip: number = 0;
  public take: number = 15;
  public translations: TranslationDTO[];
  public uiElements: UiElementsDto[];
  private uiElementsSubscription: Subscription;

  constructor(
    private readonly translationsService: TranslationsService,
    private readonly uiElementsService: UiElementsService
  ) {
  }

  ngOnInit() {
    this.translationsService.getAllTranslations(this.skip, this.take)
      .subscribe((data: TranslationDTO[]) => this.translations = data);
    this.uiElementsSubscription = this.uiElementsService.uiElementsData$.subscribe((data) => {
      this.uiElements = data;
    });
  }

  ngOnDestroy(): void {
    this.uiElementsSubscription.unsubscribe();
  }

  loadMoreTranslations(): void {
    this.skip += 15;
    this.translationsService.getAllTranslations(this.skip, this.take).subscribe((data: TranslationDTO[]) => {
      this.translations = this.translations.concat(data);
    });
  }

  searchForTranslation(value): void {
    this.translationsService.getTranslationByContent(value).subscribe((data) => {
      this.translations = data;
    });
  }
}
