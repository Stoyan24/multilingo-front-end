import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllTranslationsComponent } from './all-translations/all-translations.component';
import { EditorGuard } from '../core/guards/editor.guard';



const routes: Routes = [
  { path: '',
  component: AllTranslationsComponent,
  canActivate: [EditorGuard],
  pathMatch: 'full'
},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TranslationsRouting {}
