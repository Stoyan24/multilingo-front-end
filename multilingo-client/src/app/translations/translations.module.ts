import {NgModule} from '@angular/core';
import {AllTranslationsComponent} from './all-translations/all-translations.component';
import {SharedModule} from '../shared/shared.module';
import {CoreModule} from '../core/core.module';
import {TranslationsRouting} from './translations.routing';
import {TranslationsTableComponent} from './translations-table/translations-table.component';
import {EditTranslationComponent} from './edit-translation/edit-translation.component';
import {InputUtilitiesModule} from 'angular-bootstrap-md';
import {NgxSpinnerModule} from 'ngx-spinner';
import {GoTopButtonModule} from 'ng2-go-top-button';


@NgModule({
  declarations: [AllTranslationsComponent, TranslationsTableComponent, EditTranslationComponent],
  imports: [
    GoTopButtonModule,
    SharedModule,
    CoreModule,
    TranslationsRouting,
    InputUtilitiesModule,
    NgxSpinnerModule,
  ],
  exports: [AllTranslationsComponent, TranslationsTableComponent, EditTranslationComponent]
})
export class TranslationsModule {
}
