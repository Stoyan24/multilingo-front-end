import { TranslationDTO } from './../model/translation.dto';
import { Component, Input, OnInit} from '@angular/core';
import { NotificationService } from '../../core/services/notification.service';
import { TranslationsService } from '../../core/services/translations.service';
import { UiElementsDto } from '../../shared/ui-elements/models/ui-elements.dto';
import { UIelFinderPipe } from '../../core/pipes/uiel-finder.pipe';

@Component({
  selector: 'app-translations-table',
  templateUrl: './translations-table.component.html',
  styleUrls: ['./translations-table.component.scss']
})
export class TranslationsTableComponent {

  @Input() public translations: TranslationDTO[];
  @Input() private uiElements: UiElementsDto[];

  constructor(
    private readonly translationService: TranslationsService,
    private readonly notification: NotificationService,
    private readonly UIelFinder: UIelFinderPipe,
  ) {
  }

  public updateTranslation(value: TranslationDTO) {
    this.translationService.editTranslation(value.id, value.translation)
      .subscribe((data: TranslationDTO) => {
        const updatedTranslation = this.translations.findIndex(
          (t: TranslationDTO) => t.id === data.id);
        this.translations[updatedTranslation].translation = data.translation;
        this.translations[updatedTranslation].rating.avgrating = data.rating.avgrating;
        this.notification.success(this.UIelFinder.transform(this.uiElements, 'translation_edited'));
      },
        (error) => this.notification.error(error.error));
  }

}
