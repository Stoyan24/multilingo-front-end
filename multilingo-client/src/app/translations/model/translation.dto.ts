import { RatingDTO } from '../../shared/rating/models/rating.dto';
export class TranslationDTO {

  public id: string;
  public originalText: string;
  public translation: string;
  public isDeleted: boolean;
  public originalLanguageCode: string;
  public targetLanguageCode: string;
  public rating: RatingDTO;

}
