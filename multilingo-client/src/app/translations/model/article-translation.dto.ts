import { RatingDTO } from './../../shared/rating/models/rating.dto';
export class ArticleTranslationDTO {

  public originalText: string;
  public translatedText: string;
  public rating: RatingDTO;

}
