import { Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslationDTO } from '../model/translation.dto';
import { UiElementsDto } from '../../shared/ui-elements/models/ui-elements.dto';

@Component({
  selector: 'app-edit-translation',
  templateUrl: './edit-translation.component.html',
  styleUrls: ['./edit-translation.component.scss']
})
export class EditTranslationComponent implements OnInit {

  public editText: FormGroup;

  @Input() private uiElements: UiElementsDto[];
  @Input() currentTranslation: TranslationDTO;
  @Output() public readonly editedText: EventEmitter<TranslationDTO> = new EventEmitter<TranslationDTO>();

  constructor(public readonly formBuilder: FormBuilder) { }

  ngOnInit() {
    this.editText = this.formBuilder.group({
      translation: [this.currentTranslation.translation, [Validators.required]]
      }
    );
  }

  emitTest(value) {
    this.currentTranslation.translation = value;
    this.editedText.emit(this.currentTranslation);
  }

}
