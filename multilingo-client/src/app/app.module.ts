import { AppRoutingModule } from './app.routing';
import { BrowserModule} from '@angular/platform-browser';
import { NgModule} from '@angular/core';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppComponent} from './app.component';
import { MDBBootstrapModule} from 'angular-bootstrap-md';
import { ToastrModule} from 'ngx-toastr';
import { CoreModule} from './core/core.module';
import { JwtModule} from '@auth0/angular-jwt';
import { AuthService } from './core/services/auth.service';
import { AuthInterceptor } from './core/interceptors/auth.interceptor';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ComponentsModule } from './components/components.module';
import { SharedModule } from './shared/shared.module';
import { SpinnerInterceptor } from './core/interceptors/spinner.interceptor';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ErrorInterceptor } from './core/interceptors/error.interceptor';
import { UiElementsService } from './core/services/ui-elements.service';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    AppRoutingModule,
    CoreModule,
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MDBBootstrapModule.forRoot(),
    ToastrModule.forRoot(),
    JwtModule.forRoot({config: {}}),
    ComponentsModule,
    SharedModule,
    NgxSpinnerModule,
  ],
  providers: [
    UiElementsService,
    AuthService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: SpinnerInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
