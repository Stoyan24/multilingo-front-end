import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {UiElementsDto} from '../../shared/ui-elements/models/ui-elements.dto';
import {UiElementsService} from '../../core/services/ui-elements.service';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss']
})
export class BannerComponent implements OnInit, OnDestroy {

  public uiElements: UiElementsDto[];
  private uiElementsSubscription: Subscription;

  constructor(private readonly uiElementsService: UiElementsService) { }

  ngOnInit() {
    this.uiElementsSubscription = this.uiElementsService.uiElementsData$.subscribe((data) => {
      this.uiElements = data;
    });
  }

  ngOnDestroy(): void {
    this.uiElementsSubscription.unsubscribe();
  }

}
