import {Component, OnDestroy, OnInit} from '@angular/core';
import {UiElementsDto} from '../../shared/ui-elements/models/ui-elements.dto';
import {Subscription} from 'rxjs';
import {UiElementsService} from '../../core/services/ui-elements.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit, OnDestroy {

  public uiElements: UiElementsDto[];
  private uiElementsSubscription: Subscription;

  constructor(private readonly uiElementsService: UiElementsService) { }

  ngOnInit() {
    this.uiElementsSubscription = this.uiElementsService.uiElementsData$.subscribe((data) => {
      this.uiElements = data;
    });
  }

  ngOnDestroy(): void {
    this.uiElementsSubscription.unsubscribe();
  }
}
