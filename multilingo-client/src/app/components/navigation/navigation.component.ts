import { LanguageDto } from './../../shared/languages/models/language.dto';
import { LanguagesService } from './../../core/services/languages.service';
import { CurrentLanguageDTO } from './../../shared/languages/models/current-language.dto';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { UserDTO } from '../../users/models/user.dto';
import { Subscription } from 'rxjs';
import { AuthService } from '../../core/services/auth.service';
import { NotificationService } from '../../core/services/notification.service';
import { Router } from '@angular/router';
import { UiElementsService } from '../../core/services/ui-elements.service';
import { UiElementsDto } from '../../shared/ui-elements/models/ui-elements.dto';
import { UIelFinderPipe } from '../../core/pipes/uiel-finder.pipe';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit, OnDestroy {

  public loggedUser: UserDTO;
  private loggedUserSubscription: Subscription;
  public uiElements: UiElementsDto[];
  private uiElementsSubscription: Subscription;


  public constructor(
    private readonly languageService: LanguagesService,
    private readonly authService: AuthService,
    private readonly notification: NotificationService,
    private readonly router: Router,
    private readonly uiElementsService: UiElementsService,
    private readonly UIelFinder: UIelFinderPipe,
  ) {}

  ngOnInit(): void {
    this.loggedUserSubscription = this.authService.loggedUserData$.subscribe((data: UserDTO) => {
      this.loggedUser = data;
    });

    this.uiElementsSubscription = this.uiElementsService.uiElementsData$.subscribe((data) => {
      this.uiElements = data;
    });
  }

  public setCurrentLanguage(language: LanguageDto) {
    const currentLanguage: CurrentLanguageDTO = {
      code: language.code,
      name: language.name,
    };

    this.uiElementsService.getAllUIElements(language.code).subscribe((data) => {
      this.uiElementsService.emitUIELementsData$(data);
    });

    this.languageService.changeCurrentLanguageCode(currentLanguage);
  }

  public signOut(): void {
    this.authService
      .signOut()
      .subscribe(
        () => {
          this.notification.success(this.UIelFinder.transform(this.uiElements, 'toaster_sign_out'));
          this.router.navigate(['/']);
        },
        () => this.notification.error(this.UIelFinder.transform(this.uiElements, 'sign_err'))
      );
  }

  ngOnDestroy(): void {
    this.uiElementsSubscription.unsubscribe();
    this.loggedUserSubscription.unsubscribe();
  }
}
