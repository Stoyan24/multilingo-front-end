import { LanguageDto } from './../../shared/languages/models/language.dto';
import { Component, OnDestroy, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { AuthService } from '../../core/services/auth.service';
import { NotificationService } from '../../core/services/notification.service';
import { Router } from '@angular/router';
import { RegisterUserDto } from './models/register-user.dto';
import { LanguagesService } from '../../core/services/languages.service';
import { UiElementsDto } from '../../shared/ui-elements/models/ui-elements.dto';
import { Subscription } from 'rxjs';
import { UiElementsService } from '../../core/services/ui-elements.service';
import { UIelFinderPipe } from '../../core/pipes/uiel-finder.pipe';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {

  public registerForm: FormGroup;
  public languages: LanguageDto[];
  public uiElements: UiElementsDto[];
  private uiElementsSubscription: Subscription;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly authService: AuthService,
    private readonly notification: NotificationService,
    private readonly router: Router,
    private readonly languageService: LanguagesService,
    private readonly uiElementsService: UiElementsService,
    private readonly UIelFinder: UIelFinderPipe,
  ) {
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
        preferedLangCode: ['', [Validators.required]
        ],
        avatarPath: ['', [Validators.required]
        ],
        username: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(20)]],
        password: ['', [Validators.required, Validators.pattern(
          /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/
        )]],
        email: ['', [Validators.required, Validators.email]]
      }
    );
    this.languageService.getAllLanguages().subscribe((languages: LanguageDto[]) => {
      this.languages = languages;
    }, (err) => {
      this.notification.error(err);
    });
    this.uiElementsSubscription = this.uiElementsService.uiElementsData$.subscribe((data) => {
      this.uiElements = data;
    });
  }


  public register(user: RegisterUserDto) {
    this.authService.register(user).subscribe(
      () => {
        this.notification.success( this.UIelFinder.transform(this.uiElements, 'toaster_reg'));
        this.router.navigate(['login']);
      },
      (error) => {
        this.notification.error(this.UIelFinder.transform(this.uiElements, 'ops'));
      }
    );
  }

  ngOnDestroy(): void {
    this.uiElementsSubscription.unsubscribe();
  }
}
