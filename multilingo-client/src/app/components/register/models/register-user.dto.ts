import {Validators} from '@angular/forms';

export class RegisterUserDto {
  public preferedLangCode: string;
  public avatarPath: string;
  public username: string;
  public password: string;
  public email: string;
}
