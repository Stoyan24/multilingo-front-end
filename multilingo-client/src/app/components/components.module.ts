import { NgModule } from '@angular/core';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { NavigationComponent } from './navigation/navigation.component';
import { SharedModule } from '../shared/shared.module';
import { HomeComponent } from './home/home.component';
import { BannerComponent } from './banner/banner.component';
import { FooterComponent } from './footer/footer.component';
import { MDBRootModule } from 'angular-bootstrap-md';
import { CoreModule } from '../core/core.module';
import { ArticlesModule } from '../articles/articles.module';
import { NotFoundPageComponent } from './not-found-page/not-found-page.component';
import { ServerErrorPageComponent } from './server-error-page/server-error-page.component';

@NgModule({

  declarations: [
    BannerComponent,
    FooterComponent,
    HomeComponent,
    LoginComponent,
    NavigationComponent,
    RegisterComponent,
    NotFoundPageComponent,
    ServerErrorPageComponent,
  ],
  imports: [
    ArticlesModule,
    SharedModule,
    CoreModule,
    MDBRootModule,
  ],
  exports: [
    BannerComponent,
    FooterComponent,
    NavigationComponent,
    NotFoundPageComponent,
    ServerErrorPageComponent
  ],
})
export class ComponentsModule { }
