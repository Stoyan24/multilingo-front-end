import { RoleDTO } from './../../users/models/role.dto';
import { UserDTO } from './../../users/models/user.dto';
import { StorageService } from './../../core/services/storage.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../core/services/auth.service';
import { NotificationService } from '../../core/services/notification.service';
import { Router } from '@angular/router';
import { LoginUserDto } from './models/login-user.dto';
import { UiElementsDto } from '../../shared/ui-elements/models/ui-elements.dto';
import { Subscription } from 'rxjs';
import { UiElementsService } from '../../core/services/ui-elements.service';
import { UIelFinderPipe } from '../../core/pipes/uiel-finder.pipe';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  public loginForm: FormGroup;

  public uiElements: UiElementsDto[];
  private uiElementsSubscription: Subscription;
  private loggedUserSubscription: Subscription;
  public loggedUserData: UserDTO;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly authService: AuthService,
    private readonly notification: NotificationService,
    private readonly router: Router,
    private readonly uiElementsService: UiElementsService,
    private readonly UIelFinder: UIelFinderPipe,
  ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      usernameOrEmail: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(20)]
      ],
      password: ['', [Validators.required, Validators.pattern(
        /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/
      )]]
    });
    this.uiElementsSubscription = this.uiElementsService.uiElementsData$.subscribe((data) => {
      this.uiElements = data;
      this.loggedUserSubscription = this.authService.loggedUserData$.subscribe(
        (userData: UserDTO) => this.loggedUserData = userData,
      );
    });
  }

  public login(user: LoginUserDto) {
    this.authService.signIn(user).subscribe(
      (data) => {
        this.notification.success(this.UIelFinder.transform(this.uiElements, 'toaster_sign_in'));
        if (this.authService.getUserDataIfAuthenticated().roles.includes('Administrator')) {
          this.router.navigate(['/admin-panel']);
        } else if (this.authService.getUserDataIfAuthenticated().roles.includes('Editor')) {
          this.router.navigate(['/translations']);
        } else {
          this.router.navigate(['/home']);
        }
      },
      (err) => {
        this.notification.error(err.error.error);
      }
    );
  }

  ngOnDestroy(): void {
    this.uiElementsSubscription.unsubscribe();
    this.loggedUserSubscription.unsubscribe();
  }
}
