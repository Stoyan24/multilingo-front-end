import { ToastrService } from 'ngx-toastr';
import { CoreModule } from './../../core/core.module';
import { UIelFinderPipe } from './../../core/pipes/uiel-finder.pipe';
import { NotificationService } from './../../core/services/notification.service';
import { AuthService } from './../../core/services/auth.service';
import { AllArticlesComponent } from './../../articles/all-articles/all-articles.component';
import { SharedModule } from './../../shared/shared.module';
import { ArticlesResolverService } from './../../core/services/articles-resolver.service';
import { Routes, Router } from '@angular/router';
import { HomeComponent } from '../home/home.component';
import { LoginComponent } from './login.component';
import { ComponentFixture, async, TestBed } from '@angular/core/testing';
import { of, interval, throwError } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import {UiElementsService} from '../../core/services/ui-elements.service';

fdescribe('LoginComponent', () => {

  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  const routes: Routes = [
    // { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', component: LoginComponent },
    { path: 'admin-panel', component: LoginComponent },
    { path: 'translations', component: LoginComponent },

  ];

  let formBuilder;
  let router;
  let notification;
  let uiElementsService;
  let UIelFinder;
  let authService;

  UIelFinder = {
    transform() {},
  };

  beforeEach(async(() => {
    jest.clearAllMocks();

    authService = {
      loggedUserData$: of(null),
      getUserDataIfAuthenticated() {
        return {
          roles: [],
        };
      },
      signIn() { },
    };

    formBuilder = {
      group() {},
    };

    router = {
      navigate() {},
    };

    notification = {
      success() {},
      error() {},
    };

    UIelFinder = {
      transform() {},
    };

    uiElementsService = {
      get uiElementsData$() { return of([]); }
    };

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes(routes),
        SharedModule,
        CoreModule,
        BrowserAnimationsModule,
        HttpClientModule,
      ],
      declarations: [ LoginComponent ],
      providers: [
        AuthService,
        FormBuilder,
        NotificationService,
        UiElementsService,
        ToastrService
      ]
    })
      .overrideProvider(AuthService, { useValue: authService })
      // .overrideProvider(FormBuilder, { useValue: formBuilder })
      .overrideProvider(NotificationService, { useValue: notification })
      .overrideProvider(ToastrService, { useValue: {} })
      .overrideProvider(UiElementsService, { useValue: uiElementsService })
      .compileComponents();

    router = TestBed.get(Router);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    component.loginForm = new FormGroup({
      usernameOrEmail: new FormControl(),
      password: new FormControl(),
    });
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call authService.signIn with the user data', () => {

    const user = {
      usernameOrEmail: 'admin',
      password: '123456789a',
    };
    const loginSpy = jest.spyOn(authService, 'signIn').mockImplementation(() => of(true));

    component.loginForm.controls.usernameOrEmail.setValue('admin');
    component.loginForm.controls.password.setValue('123456789a');

    component.login(user);

    fixture.detectChanges();

    expect(loginSpy).toHaveBeenCalledWith(user);

  });

  it('login should call authService.signIn with user data', () => {

    const user = {
      usernameOrEmail: 'admin',
      password: '123456789a',
    };
    const loginSpy = jest.spyOn(authService, 'signIn').mockImplementation(() => of(true));

    component.loginForm.controls.usernameOrEmail.setValue('admin');
    component.loginForm.controls.password.setValue('123456789a');

    component.login(user);

    fixture.detectChanges();

    expect(loginSpy).toHaveBeenCalledTimes(1);

  });

  it('successfull login should navigate to /home', () => {

    const user = {
      usernameOrEmail: 'admin',
      password: '123456789a',
    };
    const loginSpy = jest.spyOn(authService, 'signIn').mockImplementation(() => of(true));
    const navigateSpy = jest.spyOn(router, 'navigate');

    component.loginForm.controls.usernameOrEmail.setValue('admin');
    component.loginForm.controls.password.setValue('123456789a');

    component.login(user);

    fixture.detectChanges();

    expect(router.navigate).toHaveBeenCalledWith(['/home']);

  });


  it('Unsuccessfull login should notify', () => {

    const user = {
      usernameOrEmail: null,
      password: null,
    };
    const error = {
      message: 'Wrong username or password'
    };

    const loginSpy = jest.spyOn(authService, 'signIn').mockImplementation(() => of(false));
    const navigateSpy = jest.spyOn(router, 'navigate');

    component.loginForm.controls.usernameOrEmail.setValue(null);
    component.loginForm.controls.password.setValue(null);

    component.login(user);

    fixture.detectChanges();

    expect(loginSpy).toHaveBeenCalledWith({
      usernameOrEmail: null,
      password: null,
    });

  });

});
