import { UiElementsService } from './../../core/services/ui-elements.service';
import { AuthService } from './../../core/services/auth.service';
import { UiElementsDto } from './../../shared/ui-elements/models/ui-elements.dto';
import { UserDTO } from './../../users/models/user.dto';
import { VersionDTO } from './../models/version.dto';
import { Component, OnInit, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-versions-table',
  templateUrl: './versions-table.component.html',
  styleUrls: ['./versions-table.component.scss']
})
export class VersionsTableComponent implements OnInit, OnDestroy {

  @Input() public versions: VersionDTO[];
  @Output() public oNSetCurrent: EventEmitter<string> = new EventEmitter<string>();
  public uiElements: UiElementsDto[];
  private uiElementsSubscription: Subscription;
  private loggedUserSubscription: Subscription;
  public loggedUserData: UserDTO;

  constructor(
    private readonly authService: AuthService,
    private readonly uiElementsService: UiElementsService,
  ) { }

  ngOnInit() {
    this.loggedUserSubscription = this.authService.loggedUserData$.subscribe(
      (data: UserDTO) => this.loggedUserData = data,
    );
    this.uiElementsSubscription = this.uiElementsService.uiElementsData$.subscribe((data) => {
      this.uiElements = data;
    });
  }

  public emitCurrent(id: string) {
    this.oNSetCurrent.emit(id);
  }

  ngOnDestroy(): void {
    this.uiElementsSubscription.unsubscribe();
    this.loggedUserSubscription.unsubscribe();
  }

}
