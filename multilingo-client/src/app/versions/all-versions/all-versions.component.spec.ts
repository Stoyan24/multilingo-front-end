import { VersionsModule } from './../versions.module';
import { AuthService } from './../../../app/core/services/auth.service';
import { UiElementsService } from './../../core/services/ui-elements.service';
import { VersionsService } from './../../core/services/versions.service';
import { NotificationService } from './../../core/services/notification.service';
import { CoreModule } from './../../core/core.module';
import { SharedModule } from './../../shared/shared.module';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AllVersionsComponent } from './all-versions.component';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

describe('AllVersionsComponent', () => {

  let toastr;
  let authService;
  let activatedRoute;
  let versionsService;
  let notificationService;
  let uiElementsService;
  let uiElFinder;
  let component: AllVersionsComponent;
  let fixture: ComponentFixture<AllVersionsComponent>;

  beforeEach(async(() => {
    jest.clearAllMocks();

    toastr = {};
    activatedRoute = {
      data: of({
        versions: [],
      }),
      snapshot: {
        params: {
          id: '1',
        },
      },
    };

    authService = {
      loggedUserData$: of([]),
    };

    versionsService = {
      getAllVersions() { },
      setCurrentVersion() { },
    };

    notificationService = {
      constructor() { },
      error() { },
      success() { },
      warning() { },
      info() { },
    };

    uiElementsService = {
      uiElementsData$: of([]),
    };

    uiElFinder = {
      transform() { },
    };
    TestBed.configureTestingModule({
      imports: [
        VersionsModule,
        RouterTestingModule,
        SharedModule,
        CoreModule,
      ],
      providers: [
        VersionsService,
        NotificationService,
        UiElementsService,
        AuthService,
      ],
      declarations: []
    })
      .overrideProvider(ActivatedRoute, { useValue: activatedRoute })
      .overrideProvider(VersionsService, { useValue: versionsService })
      .overrideProvider(NotificationService, { useValue: notificationService })
      .overrideProvider(UiElementsService, { useValue: uiElementsService })
      .overrideProvider(AuthService, { useValue: authService })
      .overrideProvider(ToastrService, { useValue: toastr })
      .compileComponents();


  }));
  beforeEach(() => {
    fixture = TestBed.createComponent(AllVersionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {

    expect(component).toBeDefined();
  });

  it('should initialize versions correctly with the data passed from the resolver', () => {

    const versions = [];

    expect(component.versions).toStrictEqual(versions);

  });

  it('should receive UI elements', () => {

    const uiEls = [];

    expect(component.uiElements).toStrictEqual(uiEls);
  });

  it('should initialize versions correctly with the data passed from the resolver', () => {

    const versionId = '1';
    const versions = [];
    const spy = jest.spyOn(component, 'setCurrent');
    const setCurrentVersionSpy = jest.spyOn(versionsService, 'setCurrentVersion')
      .mockImplementation(() => of([]));

    component.setCurrent(versionId);
    fixture.detectChanges();

    expect(component.versions).toStrictEqual(versions);

  });

});
