import { NotificationService } from './../../core/services/notification.service';
import { VersionsService } from '../../core/services/versions.service';
import { Component, OnInit, Input, OnDestroy} from '@angular/core';
import { VersionDTO } from '../models/version.dto';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { UiElementsDto } from '../../shared/ui-elements/models/ui-elements.dto';
import { UiElementsService } from '../../core/services/ui-elements.service';

@Component({
  selector: 'app-all-versions',
  templateUrl: './all-versions.component.html',
  styleUrls: ['./all-versions.component.scss']
})
export class AllVersionsComponent implements OnInit, OnDestroy {

  @Input() public versions: VersionDTO[];
  public uiElements: UiElementsDto[];
  private uiElementsSubscription: Subscription;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly notification: NotificationService,
    private readonly versionsService: VersionsService,
    private readonly uiElementsService: UiElementsService,
  ) { }

  ngOnInit() {
    this.route.data.subscribe(
      ({ versions }) => {
        this.versions = versions;
        this.orderByVersion();
      },
    );
    this.uiElementsSubscription = this.uiElementsService.uiElementsData$.subscribe((data) => {
      this.uiElements = data;
    });
  }

  public setCurrent(versionId: string) {
    const articleId = this.route.snapshot.params.id;
    this.versionsService.setCurrentVersion(articleId, versionId)
      .subscribe(
        (data) => {
          this.versions = data;
          this.orderByVersion();
        },
        (error) => this.notification.error(error.error.error),
      );
  }

  private orderByVersion() {
    this.versions.sort((a, b) => b.version - a.version);
  }

  ngOnDestroy(): void {
    this.uiElementsSubscription.unsubscribe();
  }

}
