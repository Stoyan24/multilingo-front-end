import { VersionsResolverService } from './services/versions-resolver.service';
import { NgModule } from '@angular/core';
import { VersionDetailsComponent } from './version-details/version-details.component';
import { AllVersionsComponent } from './all-versions/all-versions.component';
import { CoreModule } from '../core/core.module';
import { SharedModule } from '../shared/shared.module';
import { VersionsTableComponent } from './versions-table/versions-table.component';



@NgModule({
  declarations: [
    AllVersionsComponent,
    VersionDetailsComponent,
    VersionsTableComponent,
  ],
  providers: [
    VersionsResolverService,
  ],
  imports: [
    CoreModule,
    SharedModule,
  ],
  exports: [
    AllVersionsComponent,
  ],
})
export class VersionsModule { }
