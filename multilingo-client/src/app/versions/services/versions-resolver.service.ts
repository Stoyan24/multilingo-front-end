import { LanguagesService } from './../../core/services/languages.service';
import { NotificationService } from './../../core/services/notification.service';
import { VersionsService } from './../../core/services/versions.service';
import { Injectable } from '@angular/core';
import { VersionDTO } from '../models/version.dto';
import { map } from 'rxjs/operators';
import { Resolve, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable()
export class VersionsResolverService implements Resolve<VersionDTO[]> {

  constructor(
    private readonly router: Router,
    private readonly versionsService: VersionsService,
    public readonly notification: NotificationService,
    public readonly languageService: LanguagesService,
  ) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<VersionDTO[]> {

    const articleId: string = route.params.id;
    return this.versionsService.getAllVersions(articleId)
      .pipe(
        map(versions => {
          if (versions) {
            return versions;
          } else {
            this.router.navigate(['/not-found']);
            this.notification.error(`There was an unexpected error.`);
            return [];
          }
        })
      );
  }

}
