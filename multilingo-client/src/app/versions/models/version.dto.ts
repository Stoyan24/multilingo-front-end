import { ArticleTranslationDTO } from './../../translations/model/article-translation.dto';
export class VersionDTO {

  public id: string;
  public imgPath: string;
  public title: string | ArticleTranslationDTO;
  public content: string | ArticleTranslationDTO;
  public version: number;
  public languageCode: string;
  public isCurrent: boolean;
  public isDeleted: boolean;

}
