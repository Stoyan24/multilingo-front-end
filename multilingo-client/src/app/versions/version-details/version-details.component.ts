import { VersionDTO } from './../models/version.dto';
import { Component, OnInit, Input } from '@angular/core';
import { UiElementsDto } from '../../shared/ui-elements/models/ui-elements.dto';

@Component({
  selector: 'app-version-details',
  templateUrl: './version-details.component.html',
  styleUrls: ['./version-details.component.scss']
})
export class VersionDetailsComponent implements OnInit {

  @Input() public uiElements: UiElementsDto[];
  @Input() public version: VersionDTO;

  constructor() { }

  ngOnInit() {}

}
